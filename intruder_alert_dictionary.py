#!/usr/bin/env python3
##################################
# intruder alert
#################################
login = {"apple" : "red", "lettuce" : "green", "lemon" : "yellow", "orange" : "orange"}
name = str(input("please enter your name: " ))
while name in login:
    password = str(input(f"wellcome, {name}. please enter your password: " ))
    if password == login.get(name):
        print("wellcome, Master")
    else:
        print("INTRUDER INTRUDER INTRUDER")
else:
    print("wrong username, please run again")
