#!/usr/bin/env python3
##################################
# age in months and verify integer
#################################

try:
    age = int(input("please enter your age: "))
    print( 'you are', age*12 ,'months old')
except ValueError:
    print("wrong data type, numbers only")
    age = int(input("please enter your age: "))
    print( 'you are', age*12 ,'months old')
