#!/usr/bin/env python3
##################################
# average
#################################
print ('this script will calculate the average of 20 grades')
# getting the grades from the user
count = 1
grades = []
while count <= 20:
    grade = input("please provide a grade: " )
    grades.append(grade)
    count += 1
# turning the list of the grades into integers
for i in range(0, len(grades)):
    grades[i] = int(grades[i])
# calculating the average
sum =  sum(grades)
average = sum / 20
print("the following grades are above average: ")
for i in grades:
    if i >= average:
        print(i)
