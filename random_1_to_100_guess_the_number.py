#!/usr/bin/env python3
##################################
# random 1 to 100
#################################
guess_the_number=int(input(" I pick a random number between 1 and 100. guess what number I pick: " ))

from random import randint
number=int(randint(1,100))
if guess_the_number > number:
    print("too big")
    print("I picked", number)
if guess_the_number < number:
    print("too small")
    print("I picked", number)
if guess_the_number == number:
    print("you are too smart for your own good")
